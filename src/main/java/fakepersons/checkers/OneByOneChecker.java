package fakepersons.checkers;

import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.util.ArrayList;
import java.util.List;

public abstract class OneByOneChecker extends Checker
{
    public OutputItem[] Check(InputItem[] input)
    {
        List<OutputItem> outputItems = new ArrayList<>();
        for(int i=0; i <input.length; i++)
        {
            InputItem inputItem = input[i];
            String message = CheckSingleItem(inputItem);
            if(message != null)
            {
                outputItems.add(new OutputItem(i,message));
            }
        }
        return outputItems.toArray(new OutputItem[0]);
    }

    public abstract String CheckSingleItem(InputItem inputItem);
}
