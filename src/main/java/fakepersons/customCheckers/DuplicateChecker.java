package fakepersons.customCheckers;

import fakepersons.checkers.Checker;
import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DuplicateChecker extends Checker
{

    @Override
    public OutputItem[] Check(InputItem[] input)
    {
        List<OutputItem> result = new ArrayList<>();
        HashMap<String, InputItem> hashMap = new HashMap<>();

        for (int i=0;i<input.length; i++)
        {
            InputItem item = input[i];
            String key = item.getIdCardNumber();
            if(hashMap.containsKey(key))
            {
                InputItem old = hashMap.get(key);
                String message =
                        "Stejné číslo dokladu, jako měl "
                                + old.getFirstName() + " "
                                + old.getSurname();
                result.add(new OutputItem(i,message));
            }
            else
            {
                if(!item.getIdCardNumber().isEmpty())
                {
                    hashMap.put(key, item);
                }
            }
        }
        return result.toArray(new OutputItem[0]);
    }

    @Override
    public String GetCheckerName()
    {
        return "Duplicitní čísla";
    }
}
