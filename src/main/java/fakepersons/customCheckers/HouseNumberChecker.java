package fakepersons.customCheckers;

import fakepersons.checkers.OneByOneChecker;
import fakepersons.data.InputItem;

public class HouseNumberChecker extends OneByOneChecker
{
    @Override
    public String GetCheckerName()
    {
        return "Číslo domu";
    }

    @Override
    public String CheckSingleItem(InputItem inputItem)
    {
        for(char c : inputItem.getHouseNumber().toCharArray())
        {
            if(!Character.isDigit(c))
            {
                return "Nečíselná hodnota: "+inputItem.getHouseNumber();
            }
        }
        return null;
    }
}
