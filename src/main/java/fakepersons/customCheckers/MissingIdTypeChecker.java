package fakepersons.customCheckers;

import fakepersons.checkers.OneByOneChecker;
import fakepersons.data.InputItem;

public class MissingIdTypeChecker extends OneByOneChecker
{
    @Override
    public String GetCheckerName()
    {
        return "Typ dokladu";
    }

    public String CheckSingleItem(InputItem inputItem)
    {
        if(inputItem.getIdCardType().length() < 1)
        {
            return "Chybí";
        }
        return null;
    }

}
